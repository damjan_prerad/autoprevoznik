
USE autoprevozna_kompanija_db;

-- POGLEDI

-- Pregled naziva svih tabela u bazi podataka
DROP TABLE IF EXISTS table_names_list;
CREATE OR REPLACE VIEW table_names_list AS
SELECT table_name
FROM information_schema.tables
WHERE table_schema ='autoprevozna_kompanija_db';

-- Pogled svih tekucih troskova
DROP TABLE IF EXISTS tekuci_troskovi_mjeseca;
CREATE OR REPLACE VIEW tekuci_troskovi_mjeseca AS
SELECT opis_troska, iznos_troska, vrijeme_i_datum_polaska AS datum
FROM dodatni_troskovi_voznje a, voznja b
WHERE YEAR(vrijeme_i_datum_polaska) = YEAR(CURRENT_DATE()) AND MONTH(Vrijeme_i_datum_polaska) = MONTH(CURRENT_DATE()) AND b.idvoznja = a.voznja_idvoznja
UNION
SELECT a.opis_troska, a.iznos_troska, a.datum_troska
FROM troskovi_odrzavanja_vozila a, vozilo b
WHERE YEAR(a.datum_troska) = YEAR(CURRENT_DATE()) AND MONTH(a.datum_troska) = MONTH(CURRENT_DATE()) AND b.Registracija = a.Vozilo_registracija;

-- Pogled svih tekucih uplata
DROP TABLE IF EXISTS tekuce_uplate_mjeseca;
CREATE OR REPLACE VIEW tekuce_uplate_mjeseca AS
SELECT a.Uplaceni_iznos, a.Datum_uplate, b.Ime_Stranke
FROM Uplata a
JOIN stranka b ON a.Stranka_Ime_stranke = b.Ime_stranke
WHERE YEAR(a.Datum_uplate) = YEAR(CURRENT_DATE()) AND MONTH(a.Datum_uplate) = MONTH(CURRENT_DATE());

-- Pogled svih troskova proslog mjeseca
DROP TABLE IF EXISTS troskovi_proslog_mjeseca;
CREATE OR REPLACE VIEW troskovi_proslog_mjeseca AS
SELECT opis_troska, iznos_troska, vrijeme_i_datum_polaska AS datum
FROM dodatni_troskovi_voznje a, voznja b
WHERE YEAR(vrijeme_i_datum_polaska) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(Vrijeme_i_datum_polaska) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) AND b.idvoznja = a.voznja_idvoznja
UNION
SELECT a.opis_troska, a.iznos_troska, a.datum_troska
FROM troskovi_odrzavanja_vozila a, vozilo b
WHERE YEAR(a.datum_troska) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(a.datum_troska) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) AND b.Registracija = a.Vozilo_registracija;

-- Pogled svih uplata proslog mjeseca
DROP TABLE IF EXISTS uplate_proslog_mjeseca;
CREATE OR REPLACE VIEW uplate_proslog_mjeseca AS
SELECT a.Uplaceni_iznos, a.Datum_uplate, b.Ime_Stranke
FROM Uplata a
JOIN stranka b ON a.Stranka_Ime_stranke = b.Ime_stranke
WHERE YEAR(a.Datum_uplate) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(a.Datum_uplate) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH);

-- Pogled svih neuplacenih voznji
DROP TABLE IF EXISTS vidi_neuplacene_voznje;
CREATE OR REPLACE VIEW vidi_neuplacene_voznje AS
SELECT *
FROM Voznja a
LEFT JOIN Uplata b
ON b.UplataID = a.Uplata_UplataID
WHERE b.Uplaceni_iznos IS NULL OR b.Uplaceni_iznos < b.Ukupni_iznos;

-- Pogled svih voznji bez dodijeljenog vozaca
DROP TABLE IF EXISTS vidi_nedodijeljene_voznje;
CREATE OR REPLACE VIEW vidi_nedodijeljene_voznje AS
SELECT *
FROM Voznja a
WHERE a.Vozac_idVozac IS NULL;

-- Pogled svih voznji bez dodijeljenog vozila
DROP TABLE IF EXISTS vidi_nedodijeljene_voznje_vozila;
CREATE OR REPLACE VIEW vidi_nedodijeljene_voznje_vozila AS
SELECT *
FROM Voznja a
WHERE a.Vozilo_Registracija IS NULL;

-- PROCEDURE

DROP PROCEDURE IF EXISTS suma_svih_troskova_ovog_mjeseca;
DELIMITER $$
CREATE PROCEDURE suma_svih_troskova_ovog_mjeseca(OUT suma FLOAT)
BEGIN
    SELECT SUM(svi_troskovi.Iznos_troska)
    INTO suma
	FROM (
		SELECT a.Iznos_troska
		FROM dodatni_troskovi_voznje a, voznja b
        WHERE YEAR(vrijeme_i_datum_polaska) = YEAR(CURRENT_DATE()) AND MONTH(Vrijeme_i_datum_polaska) = MONTH(CURRENT_DATE()) AND b.idvoznja = a.voznja_idvoznja
		UNION
		SELECT a.Iznos_troska
		FROM troskovi_odrzavanja_vozila a, vozilo b
        WHERE YEAR(a.datum_troska) = YEAR(CURRENT_DATE()) AND MONTH(a.datum_troska) = MONTH(CURRENT_DATE()) AND b.Registracija = a.Vozilo_registracija
        ) AS svi_troskovi;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS suma_svih_uplata_ovog_mjeseca;
DELIMITER $$
CREATE PROCEDURE suma_svih_uplata_ovog_mjeseca(OUT suma FLOAT)
BEGIN
    SELECT SUM(Uplaceni_iznos)
    INTO suma
	FROM Uplata
	WHERE YEAR(datum_uplate) = YEAR(CURRENT_DATE()) AND MONTH(datum_uplate) = MONTH(CURRENT_DATE());
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS suma_svih_troskova_proslog_mjeseca;
DELIMITER $$
CREATE PROCEDURE suma_svih_troskova_proslog_mjeseca(OUT suma FLOAT)
BEGIN
    SELECT SUM(svi_troskovi.iznos_troska)
    INTO suma
	FROM (
		SELECT opis_troska, iznos_troska, vrijeme_i_datum_polaska AS datum
		FROM dodatni_troskovi_voznje a, voznja b
		WHERE YEAR(vrijeme_i_datum_polaska) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(Vrijeme_i_datum_polaska) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) AND b.idvoznja = a.voznja_idvoznja
		UNION
		SELECT a.opis_troska, a.iznos_troska, a.datum_troska
		FROM troskovi_odrzavanja_vozila a, vozilo b
		WHERE YEAR(a.datum_troska) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(a.datum_troska) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) AND b.Registracija = a.Vozilo_registracija
        ) AS svi_troskovi;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS suma_svih_uplata_proslog_mjeseca;
DELIMITER $$
CREATE PROCEDURE suma_svih_uplata_proslog_mjeseca(OUT suma FLOAT)
BEGIN
    SELECT SUM(Uplaceni_iznos)
    INTO suma
	FROM Uplata
	WHERE YEAR(datum_uplate) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(datum_uplate) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH);
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS nadji_id_metoda_uplate
DELIMITER $$
CREATE PROCEDURE nadji_id_metoda_uplate(IN naziv_metoda_uplate VARCHAR(45), OUT id INT)
BEGIN
	SELECT a.idMetod_uplate
    INTO id
    FROM metod_uplate a
    WHERE naziv_metoda_uplate = a.Naziv_metoda_uplate;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS nova_uplata;
DELIMITER $$
CREATE PROCEDURE nova_uplata(IN ui FLOAT, IN krzu DATE, IN muiu INT, IN si VARCHAR(45))
BEGIN
	INSERT INTO uplata(ukupni_iznos, krajnji_rok_za_uplatu, metod_uplate_idmetod_uplate, Stranka_Ime_stranke)
	VALUES (ui, krzu, muiu, si);
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS organizacija_nove_voznje_na_postojecu_uplatu;
DELIMITER $$
CREATE PROCEDURE organizacija_nove_voznje_na_postojecu_uplatu(IN vidp DATETIME, IN vidd DATETIME, IN mp VARCHAR(60), IN odr VARCHAR(60), IN ud INT, IN br_put INT, IN td VARCHAR(60), IN id_uplate INT)
BEGIN
	INSERT INTO voznja(Vrijeme_i_datum_polaska, Vrijeme_i_datum_dolaska, Mjesto_polaska, Odrediste, Udaljenost, Broj_putnika, Tip_destinacije, Tip_isplate_idTip_isplate, Uplata_UplataID)
	VALUES (vidp, vidd, mp, odr, ud, br_put, td, 1, id_uplate);
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS organizacija_nove_voznje;
DELIMITER $$
CREATE PROCEDURE organizacija_nove_voznje(IN ui FLOAT, IN krzu DATE, IN muiu INT, IN si VARCHAR(45),		IN vidp DATETIME, IN vidd DATETIME, IN mp VARCHAR(60), IN odr VARCHAR(60), IN ud INT, IN br_put INT, IN td VARCHAR(60))
BEGIN
	START TRANSACTION;
		INSERT INTO uplata(ukupni_iznos, krajnji_rok_za_uplatu, metod_uplate_idmetod_uplate, Stranka_Ime_stranke)
		VALUES (ui, krzu, muiu, si);
        
        SET @last_id = (SELECT UplataID FROM Uplata WHERE UplataID = ( SELECT MAX(UplataID) FROM Uplata));
		
        INSERT INTO voznja(Vrijeme_i_datum_polaska, Vrijeme_i_datum_dolaska, Mjesto_polaska, Odrediste, Udaljenost, Broj_putnika, Tip_destinacije, Tip_isplate_idTip_isplate, Uplata_UplataID)
		VALUES (vidp, vidd, mp, odr, ud, br_put, td, 1, @last_id);
    COMMIT;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS unos_nove_stranke
DELIMITER $$
CREATE PROCEDURE unos_nove_stranke(IN istr VARCHAR(45), IN bt VARCHAR(45), IN adr VARCHAR(45))
BEGIN
	INSERT INTO Stranka(Ime_Stranke, Broj_telefona, Adresa)
    VALUES (istr, bt, adr);
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS doplata_uplate
DELIMITER $$
CREATE PROCEDURE doplata_uplate(IN iznos_doplate INT, IN id_voznja INT)
BEGIN

	IF NOT EXISTS (SELECT Uplata_UplataID FROM Voznja WHERE idVoznja = id_voznja) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "Voznja sa tim ID-em ne postoji!";
    END IF;

	UPDATE uplata
	SET 
		Uplaceni_iznos =  CASE
							WHEN Uplaceni_iznos IS NOT NULL THEN 
								Uplaceni_iznos + iznos_doplate
							ELSE iznos_doplate END,
		Datum_uplate = CURRENT_DATE()
WHERE UplataID = (SELECT Uplata_UplataID FROM Voznja WHERE idVoznja = id_voznja);
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS ima_neuplacenih_voznji
DELIMITER $$
CREATE PROCEDURE ima_neuplacenih_voznji(OUT broj_neuplacenih INT)
BEGIN
	SELECT COUNT(idVoznja)
    INTO broj_neuplacenih
    FROM Voznja a
    LEFT JOIN Uplata b
    ON b.UplataID = a.Uplata_UplataID
    WHERE b.Uplaceni_iznos IS NULL OR b.Uplaceni_iznos < b.Ukupni_iznos;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS ima_nedodijeljenih_voznji
DELIMITER $$
CREATE PROCEDURE ima_nedodijeljenih_voznji(OUT broj_nedodijeljenih INT)
BEGIN
	SELECT COUNT(idVoznja)
    INTO broj_nedodijeljenih
    FROM Voznja a
    WHERE a.Vozac_idVozac IS NULL;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS dodijeli_voznju
DELIMITER $$
CREATE PROCEDURE dodijeli_voznju(IN id_voznje INT, IN id_vozaca INT)
BEGIN
	UPDATE Voznja
    SET Vozac_idVozac = id_vozaca
    WHERE idVoznja = id_voznje;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS dodijeli_vozilo
DELIMITER $$
CREATE PROCEDURE dodijeli_vozilo(IN id_voznje INT, IN id_vozila VARCHAR(45))
BEGIN
	UPDATE Voznja
    SET Vozilo_Registracija = id_vozila
    WHERE idVoznja = id_voznje;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS ima_voznji_bez_vozila
DELIMITER $$
CREATE PROCEDURE ima_voznji_bez_vozila(OUT broj_nedodijeljenih INT)
BEGIN
	SELECT COUNT(idVoznja)
    INTO broj_nedodijeljenih
    FROM Voznja a
    WHERE a.Vozilo_Registracija IS NULL;
END$$
DELIMITER ;

-- Trigeri / okidaci
DROP TRIGGER IF EXISTS provjeri_da_li_postoji_stranka_sa_istim_imenom;
DELIMITER $$
CREATE TRIGGER provjeri_da_li_postoji_stranka_sa_istim_imenom
	BEFORE INSERT ON Stranka FOR EACH ROW
    BEGIN
    IF NEW.Ime_stranke = "" THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "Ime stranke ne moze biti prazno!";
    END IF;
    
    IF EXISTS(SELECT * FROM Stranka WHERE Ime_stranke = NEW.Ime_stranke) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "Stranka pod tim nazivom vec postoji!";
    END IF;
    END$$
DELIMITER ;

DROP TRIGGER IF EXISTS prije_ubacivanja_nove_uplate;
DELIMITER $$
CREATE TRIGGER prije_ubacivanja_nove_uplate
	BEFORE INSERT ON uplata FOR EACH ROW
    BEGIN
		IF NEW.ukupni_iznos < 0 THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "Ukupni iznos ne smije biti manji od 0!";
		END IF;
        
        if NOT EXISTS(SELECT * FROM Stranka WHERE Ime_stranke = NEW.Stranka_Ime_stranke) THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "Stranka pod tim nazivom ne postoji!";
        END IF;
        
	END$$
DELIMITER ;

	
DROP TRIGGER IF EXISTS prije_ubacivanja_nove_voznje
DELIMITER $$
CREATE TRIGGER prije_ubacivanja_nove_voznje
	BEFORE INSERT ON voznja FOR EACH ROW
    BEGIN
		IF NEW.Vrijeme_i_datum_polaska > NEW.Vrijeme_i_datum_dolaska THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "Vrijeme polaska ne moze biti nakon vremena dolaska!";
		END IF;
        
        SET @max_dostupno = 0;
        SELECT MAX(Broj_mjesta_za_putnike)
        INTO @max_dostupno
		FROM
			(SELECT Broj_mjesta_za_putnike, Registracija, Vozilo_Registracija, vrijeme_i_datum_polaska, vrijeme_i_datum_dolaska
            FROM Vozilo a
            LEFT JOIN Voznja b
            ON a.Registracija = b.Vozilo_Registracija) AS vozila_u_upotrebi
		WHERE vrijeme_i_datum_dolaska IS NULL OR vrijeme_i_datum_polaska IS NULL OR
        ((NEW.vrijeme_i_datum_polaska > vrijeme_i_datum_polaska OR vrijeme_i_datum_polaska > NEW.vrijeme_i_datum_dolaska) AND
        (vrijeme_i_datum_polaska > NEW.vrijeme_i_datum_polaska OR NEW.vrijeme_i_datum_polaska > vrijeme_i_datum_dolaska) AND
        (NEW.vrijeme_i_datum_polaska > vrijeme_i_datum_polaska OR vrijeme_i_datum_dolaska > NEW.vrijeme_i_datum_dolaska) AND
        (vrijeme_i_datum_polaska > NEW.vrijeme_i_datum_polaska OR NEW.vrijeme_i_datum_dolaska > vrijeme_i_datum_dolaska));
        
        IF @max_dostupno < NEW.broj_putnika THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "U datom terminu nema dostupnih vozila sa potrebnim brojem mjesta ili ne postoji vozilo koje moze da preveze taj broj putnika";
		END IF;
	END$$
DELIMITER ;

DROP TRIGGER IF EXISTS prije_dodijele_vozaca_voznji
DELIMITER $$
CREATE TRIGGER prije_dodijele_vozaca_voznji
BEFORE UPDATE ON voznja FOR EACH ROW
BEGIN
	IF OLD.Vozac_idVozac IS NOT NULL AND NEW.Vozilo_Registracija IS NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "Voznja vec ima dodijeljenog vozaca";
    END IF;
    IF OLD.Vozilo_Registracija IS NOT NULL AND NEW.Vozac_idVozac IS NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "Voznja vec ima dodijeljeno vozilo";
    END IF;
    IF NOT EXISTS( SELECT * FROM Vozac WHERE idVozac = NEW.Vozac_idVozac) AND  NEW.Vozilo_Registracija IS NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "Vozac sa tim ID-jem ne postoji";
    END IF;
    IF NOT EXISTS( SELECT * FROM Vozilo WHERE Registracija = NEW.Vozilo_Registracija) AND  NEW.Vozac_idVozac IS NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "Vozilo sa tom registracijom ne postoji";
    END IF;
END$$
DELIMITER ;