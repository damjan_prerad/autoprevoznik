-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema Autoprevozna_kompanija_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Autoprevozna_kompanija_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Autoprevozna_kompanija_db` DEFAULT CHARACTER SET utf8 ;
USE `Autoprevozna_kompanija_db` ;

-- -----------------------------------------------------
-- Table `Autoprevozna_kompanija_db`.`Autoprevozna_kompanija`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Autoprevozna_kompanija_db`.`Autoprevozna_kompanija` (
  `Naziv_kompanije` VARCHAR(45) NOT NULL,
  `Adresa` VARCHAR(45) NULL,
  `Broj_telefona` VARCHAR(45) NULL,
  PRIMARY KEY (`Naziv_kompanije`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Autoprevozna_kompanija_db`.`Zaposleni`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Autoprevozna_kompanija_db`.`Zaposleni` (
  `idZaposleni` INT NOT NULL AUTO_INCREMENT,
  `Ime` VARCHAR(45) NOT NULL,
  `Prezime` VARCHAR(45) NOT NULL,
  `Adresa_stanovanja` VARCHAR(45) NOT NULL,
  `Broj_telefona` VARCHAR(45) NOT NULL,
  `Autoprevozna_kompanija_Naziv_kompanije` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idZaposleni`, `Autoprevozna_kompanija_Naziv_kompanije`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Autoprevozna_kompanija_db`.`Tip_isplate`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Autoprevozna_kompanija_db`.`Tip_isplate` (
  `idTip_isplate` INT NOT NULL AUTO_INCREMENT,
  `Naziv` VARCHAR(45) NULL,
  `Dnevni_iznos` FLOAT NULL,
  PRIMARY KEY (`idTip_isplate`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Autoprevozna_kompanija_db`.`Uplata`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Autoprevozna_kompanija_db`.`Uplata` (
  `UplataID` INT NOT NULL AUTO_INCREMENT,
  `Uplaceni_iznos` FLOAT NULL,
  `Datum_uplate` DATE NULL,
  `Ukupni_iznos` FLOAT NULL,
  `Krajnji_rok_za_uplatu` DATE NOT NULL,
  `Metod_uplate_idMetod_uplate` INT NOT NULL,
  `Stranka_idStranka` INT NOT NULL,
  PRIMARY KEY (`UplataID`, `Stranka_idStranka`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Autoprevozna_kompanija_db`.`Voznja`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Autoprevozna_kompanija_db`.`Voznja` (
  `idVoznja` INT NOT NULL AUTO_INCREMENT,
  `Vrijeme_i_datum_polaska` DATETIME NULL,
  `Vrijeme_i_datum_dolaska` DATETIME NULL,
  `Mjesto_polaska` VARCHAR(60) NULL,
  `Odrediste` VARCHAR(60) NULL,
  `Udaljenost` INT NULL,
  `Broj_putnika` INT NULL,
  `Tip_destinacije` VARCHAR(45) NULL,
  `Vozac_idVozac` INT NOT NULL,
  `Vozilo_Registracija` VARCHAR(45) NOT NULL,
  `Tip_isplate_idTip_isplate` INT NOT NULL,
  `Uplata_UplataID` INT NOT NULL,
  PRIMARY KEY (`idVoznja`, `Uplata_UplataID`),
  INDEX `fk_Voznja_Tip_isplate1_idx` (`Tip_isplate_idTip_isplate` ASC) VISIBLE,
  INDEX `fk_Voznja_Uplata1_idx` (`Uplata_UplataID` ASC) VISIBLE,
  CONSTRAINT `fk_Voznja_Tip_isplate1`
    FOREIGN KEY (`Tip_isplate_idTip_isplate`)
    REFERENCES `Autoprevozna_kompanija_db`.`Tip_isplate` (`idTip_isplate`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Voznja_Uplata1`
    FOREIGN KEY (`Uplata_UplataID`)
    REFERENCES `Autoprevozna_kompanija_db`.`Uplata` (`UplataID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Autoprevozna_kompanija_db`.`Vozac`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Autoprevozna_kompanija_db`.`Vozac` (
  `idVozac` INT NOT NULL AUTO_INCREMENT,
  `Menadzer_idMenadzer` INT NOT NULL,
  `Zaposleni_idZaposleni` INT NOT NULL,
  `broj_licence` VARCHAR(45) NULL,
  PRIMARY KEY (`idVozac`, `Zaposleni_idZaposleni`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Autoprevozna_kompanija_db`.`Isplata_vozaca`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Autoprevozna_kompanija_db`.`Isplata_vozaca` (
  `IsplataID` INT NOT NULL AUTO_INCREMENT,
  `Isplaceni_iznos` FLOAT NOT NULL,
  `Datum_isplate` DATE NOT NULL,
  `Voznja_idVoznja` INT NOT NULL,
  `Vozac_idVozac` INT NOT NULL,
  `Autoprevozna_kompanija_Naziv_kompanije` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`IsplataID`, `Autoprevozna_kompanija_Naziv_kompanije`, `Vozac_idVozac`, `Voznja_idVoznja`),
  INDEX `fk_Isplata_vozaca_Voznja1_idx` (`Voznja_idVoznja` ASC) VISIBLE,
  INDEX `fk_Isplata_vozaca_Vozac1_idx` (`Vozac_idVozac` ASC) VISIBLE,
  INDEX `fk_Isplata_vozaca_Autoprevozna_kompanija1_idx` (`Autoprevozna_kompanija_Naziv_kompanije` ASC) VISIBLE,
  CONSTRAINT `fk_Isplata_vozaca_Voznja1`
    FOREIGN KEY (`Voznja_idVoznja`)
    REFERENCES `Autoprevozna_kompanija_db`.`Voznja` (`idVoznja`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Isplata_vozaca_Vozac1`
    FOREIGN KEY (`Vozac_idVozac`)
    REFERENCES `Autoprevozna_kompanija_db`.`Vozac` (`idVozac`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Isplata_vozaca_Autoprevozna_kompanija1`
    FOREIGN KEY (`Autoprevozna_kompanija_Naziv_kompanije`)
    REFERENCES `Autoprevozna_kompanija_db`.`Autoprevozna_kompanija` (`Naziv_kompanije`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Autoprevozna_kompanija_db`.`Vozni_park`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Autoprevozna_kompanija_db`.`Vozni_park` (
  `idVozni_park` INT NOT NULL AUTO_INCREMENT,
  `Kapacitet` INT NULL,
  `Autoprevozna_kompanija_Naziv_kompanije` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idVozni_park`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Autoprevozna_kompanija_db`.`Vozilo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Autoprevozna_kompanija_db`.`Vozilo` (
  `Registracija` VARCHAR(45) NOT NULL,
  `Tip_vozila` VARCHAR(45) NOT NULL,
  `Presao_kilometara` INT NULL,
  `Trosi_po_kilometru` FLOAT NOT NULL,
  `Broj_mjesta_za_putnike` VARCHAR(45) NOT NULL,
  `Mehanicar_idMehanicar` INT NOT NULL,
  `Vozni_park_idVozni_park` INT NOT NULL,
  PRIMARY KEY (`Registracija`),
  INDEX `fk_Vozilo_Vozni_park1_idx` (`Vozni_park_idVozni_park` ASC) VISIBLE,
  CONSTRAINT `fk_Vozilo_Vozni_park1`
    FOREIGN KEY (`Vozni_park_idVozni_park`)
    REFERENCES `Autoprevozna_kompanija_db`.`Vozni_park` (`idVozni_park`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Autoprevozna_kompanija_db`.`Stranka`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Autoprevozna_kompanija_db`.`Stranka` (
  `idStranka` INT NOT NULL AUTO_INCREMENT,
  `Ime_stranke` VARCHAR(45) NULL,
  `Broj_telefona` VARCHAR(45) NULL,
  `Adresa` VARCHAR(45) NULL,
  `Autoprevozna_kompanija_Naziv_kompanije` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idStranka`),
  INDEX `fk_Stranka_Autoprevozna_kompanija1_idx` (`Autoprevozna_kompanija_Naziv_kompanije` ASC) VISIBLE,
  CONSTRAINT `fk_Stranka_Autoprevozna_kompanija1`
    FOREIGN KEY (`Autoprevozna_kompanija_Naziv_kompanije`)
    REFERENCES `Autoprevozna_kompanija_db`.`Autoprevozna_kompanija` (`Naziv_kompanije`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Autoprevozna_kompanija_db`.`Metod_uplate`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Autoprevozna_kompanija_db`.`Metod_uplate` (
  `idMetod_uplate` INT NOT NULL AUTO_INCREMENT,
  `Naziv_metoda_uplate` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idMetod_uplate`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Autoprevozna_kompanija_db`.`Vlasnik`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Autoprevozna_kompanija_db`.`Vlasnik` (
  `idVlasnik` INT NOT NULL AUTO_INCREMENT,
  `Zaposleni_idZaposleni` INT NOT NULL,
  PRIMARY KEY (`idVlasnik`, `Zaposleni_idZaposleni`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Autoprevozna_kompanija_db`.`Mehanicar`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Autoprevozna_kompanija_db`.`Mehanicar` (
  `idMehanicar` INT NOT NULL AUTO_INCREMENT,
  `Menadzer_idMenadzer` INT NOT NULL,
  `Zaposleni_idZaposleni` INT NOT NULL,
  PRIMARY KEY (`idMehanicar`, `Zaposleni_idZaposleni`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Autoprevozna_kompanija_db`.`Menadzer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Autoprevozna_kompanija_db`.`Menadzer` (
  `idMenadzer` INT NOT NULL AUTO_INCREMENT,
  `Zaposleni_idZaposleni` INT NOT NULL,
  `Vlasnik_idVlasnik` INT NOT NULL,
  PRIMARY KEY (`idMenadzer`, `Zaposleni_idZaposleni`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Autoprevozna_kompanija_db`.`Isplata_mehanicara`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Autoprevozna_kompanija_db`.`Isplata_mehanicara` (
  `idIsplata_mehanicara` INT NOT NULL AUTO_INCREMENT,
  `Datum_isplate` DATE NOT NULL,
  `Iznos` FLOAT NOT NULL,
  `Autoprevozna_kompanija_Naziv_kompanije` VARCHAR(45) NOT NULL,
  `Mehanicar_idMehanicar` INT NOT NULL,
  PRIMARY KEY (`idIsplata_mehanicara`, `Autoprevozna_kompanija_Naziv_kompanije`, `Mehanicar_idMehanicar`),
  INDEX `fk_Isplata_mehanicara_i_menadzera_Autoprevozna_kompanija1_idx` (`Autoprevozna_kompanija_Naziv_kompanije` ASC) VISIBLE,
  INDEX `fk_Isplata_mehanicara_Mehanicar1_idx` (`Mehanicar_idMehanicar` ASC) VISIBLE,
  CONSTRAINT `fk_Isplata_mehanicara_i_menadzera_Autoprevozna_kompanija1`
    FOREIGN KEY (`Autoprevozna_kompanija_Naziv_kompanije`)
    REFERENCES `Autoprevozna_kompanija_db`.`Autoprevozna_kompanija` (`Naziv_kompanije`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Isplata_mehanicara_Mehanicar1`
    FOREIGN KEY (`Mehanicar_idMehanicar`)
    REFERENCES `Autoprevozna_kompanija_db`.`Mehanicar` (`idMehanicar`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Autoprevozna_kompanija_db`.`Dodatni_troskovi_voznje`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Autoprevozna_kompanija_db`.`Dodatni_troskovi_voznje` (
  `troskoviID` INT NOT NULL AUTO_INCREMENT,
  `Opis_troska` VARCHAR(1000) NULL,
  `Iznos_troska` FLOAT NULL,
  `Voznja_idVoznja` INT NOT NULL,
  PRIMARY KEY (`troskoviID`, `Voznja_idVoznja`),
  CONSTRAINT `fk_Dodatni_troskovi_voznje_Voznja1`
    FOREIGN KEY (`Voznja_idVoznja`)
    REFERENCES `Autoprevozna_kompanija_db`.`Voznja` (`idVoznja`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Autoprevozna_kompanija_db`.`Komentari_vozaca`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Autoprevozna_kompanija_db`.`Komentari_vozaca` (
  `komentarID` INT NOT NULL AUTO_INCREMENT,
  `Voznja_idVoznja` INT NOT NULL,
  `Vozac_idVozac` INT NOT NULL,
  `Opis_komentara` VARCHAR(1000) NULL,
  PRIMARY KEY (`komentarID`, `Voznja_idVoznja`, `Vozac_idVozac`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Autoprevozna_kompanija_db`.`Posebni_zahtijevi_voznje`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Autoprevozna_kompanija_db`.`Posebni_zahtijevi_voznje` (
  `zahtijevID` INT NOT NULL AUTO_INCREMENT,
  `Opis_zahtijeva` VARCHAR(1000) NULL,
  `Voznja_idVoznja` INT NOT NULL,
  INDEX `fk_Posebni_zahtijevi_voznje_Voznja1_idx` (`Voznja_idVoznja` ASC) VISIBLE,
  PRIMARY KEY (`zahtijevID`, `Voznja_idVoznja`),
  CONSTRAINT `fk_Posebni_zahtijevi_voznje_Voznja1`
    FOREIGN KEY (`Voznja_idVoznja`)
    REFERENCES `Autoprevozna_kompanija_db`.`Voznja` (`idVoznja`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Autoprevozna_kompanija_db`.`Troskovi_odrzavanja_vozila`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Autoprevozna_kompanija_db`.`Troskovi_odrzavanja_vozila` (
  `troskoviID` INT NOT NULL AUTO_INCREMENT,
  `Vozilo_Registracija` VARCHAR(45) NOT NULL,
  `Opis_troska` VARCHAR(1000) NULL,
  `Iznos_troska` FLOAT NULL,
  `Datum_troska` DATE NULL,
  PRIMARY KEY (`troskoviID`, `Vozilo_Registracija`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Autoprevozna_kompanija_db`.`Zalbe_i_pohvale_vozaca`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Autoprevozna_kompanija_db`.`Zalbe_i_pohvale_vozaca` (
  `Zalbe_i_pohvale_vozacaID` INT NOT NULL AUTO_INCREMENT,
  `Tijelo_teksta` VARCHAR(1000) NOT NULL,
  `Stranka_idStranka` INT NOT NULL,
  `Vozac_idVozac` INT NOT NULL,
  PRIMARY KEY (`Zalbe_i_pohvale_vozacaID`, `Stranka_idStranka`, `Vozac_idVozac`),
  INDEX `fk_Zalbe_i_pohvale_vozaca_Vozac1_idx` (`Vozac_idVozac` ASC) VISIBLE,
  CONSTRAINT `fk_Zalbe_i_pohvale_vozaca_Vozac1`
    FOREIGN KEY (`Vozac_idVozac`)
    REFERENCES `Autoprevozna_kompanija_db`.`Vozac` (`idVozac`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Autoprevozna_kompanija_db`.`Isplata_menadzera`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Autoprevozna_kompanija_db`.`Isplata_menadzera` (
  `idIsplata_menadzera` INT NOT NULL AUTO_INCREMENT,
  `Datum_isplate` DATE NULL,
  `Iznos` FLOAT NULL,
  `Menadzer_idMenadzer` INT NOT NULL,
  `Autoprevozna_kompanija_Naziv_kompanije` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idIsplata_menadzera`, `Autoprevozna_kompanija_Naziv_kompanije`, `Menadzer_idMenadzer`),
  INDEX `fk_Isplata_menadzera_Menadzer1_idx` (`Menadzer_idMenadzer` ASC) VISIBLE,
  INDEX `fk_Isplata_menadzera_Autoprevozna_kompanija1_idx` (`Autoprevozna_kompanija_Naziv_kompanije` ASC) VISIBLE,
  CONSTRAINT `fk_Isplata_menadzera_Menadzer1`
    FOREIGN KEY (`Menadzer_idMenadzer`)
    REFERENCES `Autoprevozna_kompanija_db`.`Menadzer` (`idMenadzer`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Isplata_menadzera_Autoprevozna_kompanija1`
    FOREIGN KEY (`Autoprevozna_kompanija_Naziv_kompanije`)
    REFERENCES `Autoprevozna_kompanija_db`.`Autoprevozna_kompanija` (`Naziv_kompanije`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
