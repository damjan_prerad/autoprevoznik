
USE autoprevozna_kompanija_db;

INSERT INTO tip_isplate(Naziv, Dnevni_iznos)
VALUES ('Dnevnica', 50), ('Poludnevnica10', 10), ('Poludnevnica20', 20), ('Poludnevnica30', 30), ('Poludnevnica40', 20);

INSERT INTO metod_uplate(Naziv_metoda_uplate)
VALUES ('Gotovinski'), ('Ziralno');

INSERT INTO autoprevozna_kompanija(Naziv_kompanije, Adresa, Broj_telefona)
VALUES ('Prerad - transport', 'Neka Adresa BB, Banja Luka', '066/170-888');

INSERT INTO zaposleni(Ime, Prezime, Adresa_stanovanja, Broj_telefona, autoprevozna_kompanija_naziv_kompanije)
VALUES ('Damjan', 'Prerad', 'Milana Toplice 18, Banja Luka', '066/170-888', 'Prerad - transport'),
	   ('Aleksandar', 'Prerad', 'Putnog odreda 34, Banja Luka', '065/647-099', 'Prerad - transport'),
       ('Darijo', 'Prerad', 'Milana Toplice 18, Banja Luka', '066/123-123', 'Prerad - transport'),
       ('Dragutin', 'Stojkovic', 'Ulica Brojavska 12, Banja Luka', '066/234-345', 'Prerad - transport'),
       ('Zivadin', 'Slobodanovic', 'Drugovska 15, Banja Luka', '066/456-567', 'Prerad - transport'),
       ('Ivan', 'Kosmar', 'Podgoricka 4, Banja Luka', '066/567-678', 'Prerad - transport'),
       ('Petar', 'Rogulj', 'Brigadskih Generala 15, Banja Luka', '066/916-285', 'Prerad - transport');
       
INSERT INTO vlasnik(zaposleni_idzaposleni)
VALUES (1);

INSERT INTO menadzer(zaposleni_idzaposleni, vlasnik_idvlasnik)
VALUES (2, 1),
	   (3, 1);
       
INSERT INTO vozac(menadzer_idmenadzer, zaposleni_idzaposleni, broj_licence)
VALUES (2, 4, '123-345-567'),
	   (2, 5, '542-523-623'),
       (3, 6, '923-725-835');
       
INSERT INTO mehanicar(menadzer_idmenadzer, zaposleni_idzaposleni)
VALUES (2, 7);

INSERT INTO vozni_park(kapacitet, autoprevozna_kompanija_naziv_kompanije)
VALUES (5, 'Prerad - transport');

INSERT INTO vozilo(registracija, tip_vozila, presao_kilometara, trosi_po_kilometru, broj_mjesta_za_putnike, mehanicar_idmehanicar, vozni_park_idvozni_park)
VALUES ('264-A-534', 'Neoplan', 180000, 0.3, 30, 1, 1),
	   ('446-D-235', 'Neoplan', 240000, 0.3, 30, 1, 1),
       ('234-G-246', 'Kombi bokser', 140000, 0.15, 10, 1, 1),
       ('782-V-456', 'Kombi mercedes', 80000, 0.1, 15, 1, 1),
       ('346-G-735', 'Man', 200000, 0.25, 36, 1, 1);

INSERT INTO troskovi_odrzavanja_vozila(vozilo_registracija, opis_troska, iznos_troska, datum_troska)
VALUES ('264-A-534', 'Novi amortizeri', 400.0, '2019-07-05'),
	   ('264-A-534', 'Pranje autobusa', 20.0, '2019-11-24'),
       ('264-A-534', 'Nova elektronika za klima centar', 500.0, '2020-02-14'),
       ('346-G-735', 'Nova brava', 250.0, '2020-02-05'),
       ('346-G-735', 'Popravak radija', 150.0, '2019-09-12'),
       ('782-V-456', 'Pranje kombija', 30.0, '2020-03-10');
       
INSERT INTO isplata_mehanicara(datum_isplate, iznos, mehanicar_idmehanicar, autoprevozna_kompanija_naziv_kompanije)
VALUES ('2019-01-01', 800.00, 1, 'Prerad - transport'),
	   ('2019-02-01', 800.00, 1, 'Prerad - transport'),
       ('2019-03-01', 800.00, 1, 'Prerad - transport'),
       ('2019-04-01', 800.00, 1, 'Prerad - transport'),
       ('2019-05-01', 800.00, 1, 'Prerad - transport'),
       ('2019-06-01', 800.00, 1, 'Prerad - transport'),
       ('2019-07-01', 800.00, 1, 'Prerad - transport'),
       ('2019-08-01', 800.00, 1, 'Prerad - transport'),
       ('2019-09-01', 800.00, 1, 'Prerad - transport'),
       ('2019-10-01', 800.00, 1, 'Prerad - transport'),
       ('2019-11-01', 800.00, 1, 'Prerad - transport'),
       ('2019-12-01', 800.00, 1, 'Prerad - transport'),
       ('2020-01-01', 800.00, 1, 'Prerad - transport'),
       ('2020-02-01', 800.00, 1, 'Prerad - transport'),
       ('2020-03-01', 800.00, 1, 'Prerad - transport');
	
INSERT INTO isplata_menadzera(datum_isplate, iznos, menadzer_idmenadzer, autoprevozna_kompanija_naziv_kompanije)
VALUES ('2019-01-01', 800.00, 1, 'Prerad - transport'),
	   ('2019-02-01', 800.00, 1, 'Prerad - transport'),
       ('2019-03-01', 800.00, 1, 'Prerad - transport'),
       ('2019-04-01', 800.00, 1, 'Prerad - transport'),
       ('2019-05-01', 800.00, 1, 'Prerad - transport'),
       ('2019-06-01', 800.00, 1, 'Prerad - transport'),
       ('2019-07-01', 800.00, 1, 'Prerad - transport'),
       ('2019-08-01', 800.00, 1, 'Prerad - transport'),
       ('2019-09-01', 800.00, 1, 'Prerad - transport'),
       ('2019-10-01', 800.00, 1, 'Prerad - transport'),
       ('2019-11-01', 800.00, 1, 'Prerad - transport'),
       ('2019-12-01', 800.00, 1, 'Prerad - transport'),
       ('2020-01-01', 800.00, 1, 'Prerad - transport'),
       ('2020-02-01', 800.00, 1, 'Prerad - transport'),
       ('2020-03-01', 800.00, 1, 'Prerad - transport'),
       ('2019-01-01', 800.00, 2, 'Prerad - transport'),
	   ('2019-02-01', 800.00, 2, 'Prerad - transport'),
       ('2019-03-01', 800.00, 2, 'Prerad - transport'),
       ('2019-04-01', 800.00, 2, 'Prerad - transport'),
       ('2019-05-01', 800.00, 2, 'Prerad - transport'),
       ('2019-06-01', 800.00, 2, 'Prerad - transport'),
       ('2019-07-01', 800.00, 2, 'Prerad - transport'),
       ('2019-08-01', 800.00, 2, 'Prerad - transport'),
       ('2019-09-01', 800.00, 2, 'Prerad - transport'),
       ('2019-10-01', 800.00, 2, 'Prerad - transport'),
       ('2019-11-01', 800.00, 2, 'Prerad - transport'),
       ('2019-12-01', 800.00, 2, 'Prerad - transport'),
       ('2020-01-01', 800.00, 2, 'Prerad - transport'),
       ('2020-02-01', 800.00, 2, 'Prerad - transport'),
       ('2020-03-01', 800.00, 2, 'Prerad - transport');

INSERT INTO stranka(autoprevozna_kompanija_naziv_kompanije, ime_stranke, broj_telefona, adresa)
VALUES ('Prerad - transport', 'FK Borac', '066/523-435', 'Borackih udruzenja 12, Banja Luka'),
	   ('Prerad - transport', 'Turisticka organizacija Travel', '065/755-756', 'Nezavisnih zemalja 34, Banja Luka'),
       ('Prerad - transport', 'Horska grupa Tradicija', '066/876-546', 'Proletera 53, Banja Luka');

INSERT INTO uplata(uplaceni_iznos, datum_uplate, ukupni_iznos, krajnji_rok_za_uplatu, metod_uplate_idmetod_uplate, stranka_idstranka)
VALUES (1200.00, '2019-12-25', 1200.00, '2019-12-25', 1,  1),
	   (300.00, '2019-11-12', 300.00, '2019-11-12', 1, 2),
       (200.00, '2020-01-20', 200.00, '2020-01-20', 1, 3);

INSERT INTO voznja(Vrijeme_i_datum_polaska, Vrijeme_i_datum_dolaska, Mjesto_polaska, Odrediste, Udaljenost, Broj_putnika, Tip_destinacije, Vozac_idVozac, Vozilo_Registracija, Tip_isplate_idTip_isplate, Uplata_UplataID)
VALUES ('2019-12-25 08:00:00', '2019-12-25 13:00:00', 'Banja Luka, stara autobuska stanica', 'Jahorina, hotel Zlatan', 200, 20, 'Turisticka', 1, '264-A-534', 1, 1),
	   ('2020-01-02 08:00:00', '2020-01-02 13:00:00', 'Jahorina, hotel Zlatan', 'Banja Luka, stara autobuska stanica', 200, 20, 'Turisticka', 1, '264-A-534', 1, 1),
	   ('2019-11-12 08:30:00', '2019-11-12 11:30:00', 'Banja Luka, stara autobuska stanica', 'Kozara', 50, 20, 'Turisticka', 2, '446-D-235', 1, 2),
       ('2019-11-12 17:30:00', '2019-11-12 19:00:00', 'Kozara', 'Banja Luka, stara autobuska stanica', 50, 20, 'Turisticka', 2, '446-D-235', 1, 2),
       ('2020-01-20 09:30:00', '2020-01-20 11:00:00', 'Banja Luka, stara autobuska stanica', 'Piskavica KUD', 50, 15, 'Dogadjaj', 3, '782-V-456', 1, 3),
       ('2019-11-12 18:00:00', '2019-11-12 19:30:00', 'Piskavica KUD', 'Banja Luka, stara autobuska stanica', 50, 15, 'Dogadjaj', 3, '782-V-456', 1, 3);

INSERT INTO dodatni_troskovi_voznje(Opis_troska, Iznos_troska, Voznja_idVoznja)
VALUES ('Placanje parkinga na Jahorini', 150.00, 1),
	   ('Dosipanje goriva', 100.00, 1);

INSERT INTO posebni_zahtijevi_voznje(Opis_zahtijeva, Voznja_idVoznja)
VALUES ('Potrebno je obezbijediti prostor za vise prtljaga', 1);
       
INSERT INTO komentari_vozaca(Voznja_idVoznja, Vozac_idVozac, Opis_komentara)
VALUES (1, 1, 'Voznja prosla uredno'),
	   (2, 1, 'Voznja prosla uredno'),
       (3, 2, 'Na putu do tamo, cuo se cudan zvuk u autobusu'),
       (4, 2, 'Zvuk je nestao'),
       (5, 4, 'Slucajno je prosuto pice u autobusu, sjediste 5'),
       (6, 5, 'Putnici trazili muziku koju nisam imao u autobusu');

INSERT INTO zalbe_i_pohvale_vozaca(Tijelo_teksta, Stranka_idStranka, Vozac_idVozac)
VALUES ('Vozac je brzo vozio, neugodna voznja', 1, 1);

INSERT INTO isplata_vozaca(isplaceni_iznos, datum_isplate, Voznja_idVoznja, Vozac_idVozac, Autoprevozna_kompanija_Naziv_kompanije)
VALUES (50.00, '2020-01-02', 1, 1, 'Prerad - transport'),
	   (50.00, '2020-01-02', 2, 1, 'Prerad - transport'),
       (50.00, '2019-11-12', 1, 2, 'Prerad - transport'),
       (50.00, '2019-11-12', 1, 2, 'Prerad - transport'),
       (50.00, '2019-11-12', 1, 3, 'Prerad - transport'),
       (50.00, '2019-11-12', 1, 3, 'Prerad - transport');
