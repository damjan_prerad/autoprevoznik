SELECT SUM(a.Iznos_troska) + SUM(b.Iznos_troska)
FROM dodatni_troskovi_voznje a, troskovi_odrzavanja_vozila b;

SELECT SUM(a.Iznos_troska)
FROM dodatni_troskovi_voznje a;

SELECT SUM(b.Iznos_troska)
FROM troskovi_odrzavanja_vozila b;

SELECT SUM(svi_troskovi.Iznos_troska)
FROM (
	SELECT a.Iznos_troska
	FROM dodatni_troskovi_voznje a
	UNION
	SELECT b.Iznos_troska
	FROM troskovi_odrzavanja_vozila b) AS svi_troskovi;

SELECT *
FROM Uplata;

SELECT SUM(Uplaceni_iznos)
FROM Uplata
WHERE YEAR(datum_uplate) = YEAR(CURRENT_DATE()) AND MONTH(datum_uplate) = MONTH(CURRENT_DATE());
		
SELECT *
FROM dodatni_troskovi_voznje;
    
SELECT opis_troska, iznos_troska, vrijeme_i_datum_polaska AS datum
FROM dodatni_troskovi_voznje a, voznja b
WHERE YEAR(vrijeme_i_datum_polaska) = YEAR(CURRENT_DATE()) AND MONTH(Vrijeme_i_datum_polaska) = MONTH(CURRENT_DATE()) AND b.idvoznja = a.voznja_idvoznja
UNION
SELECT a.opis_troska, a.iznos_troska, a.datum_troska
FROM troskovi_odrzavanja_vozila a, vozilo b
WHERE YEAR(a.datum_troska) = YEAR(CURRENT_DATE()) AND MONTH(a.datum_troska) = MONTH(CURRENT_DATE()) AND b.Registracija = a.Vozilo_registracija;

SELECT a.Uplaceni_iznos, a.Datum_uplate, b.Ime_Stranke
FROM Uplata a
JOIN stranka b ON a.Stranka_idStranka = b.idStranka
WHERE YEAR(a.Datum_uplate) = YEAR(CURRENT_DATE()) AND MONTH(a.Datum_uplate) = MONTH(CURRENT_DATE());

DELETE FROM uplata;

CALL suma_svih_troskova_ovog_mjeseca(@out_value);
SELECT @out_value;

SELECT *
FROM uplate_proslog_mjeseca;

SELECT opis_troska, iznos_troska, vrijeme_i_datum_polaska AS datum
FROM dodatni_troskovi_voznje a, voznja b
WHERE YEAR(vrijeme_i_datum_polaska) = YEAR(CURRENT_DATE()) AND MONTH(Vrijeme_i_datum_polaska) = MONTH(CURRENT_DATE()) - 1 AND b.idvoznja = a.voznja_idvoznja
UNION
SELECT a.opis_troska, a.iznos_troska, a.datum_troska
FROM troskovi_odrzavanja_vozila a, vozilo b
WHERE YEAR(a.datum_troska) = YEAR(CURRENT_DATE()) AND MONTH(a.datum_troska) = MONTH(CURRENT_DATE()) - 1 AND b.Registracija = a.Vozilo_registracija;

INSERT INTO uplata(ukupni_iznos, krajnji_rok_za_uplatu, metod_uplate_idmetod_uplate, Stranka_Ime_stranke)
VALUES (300.00, "2020-04-02", 1, "Horska grupa Tradicija");

SELECT * FROM Uplata;

SET @last_id = (SELECT UplataID FROM Uplata WHERE UplataID = ( SELECT MAX(UplataID) FROM Uplata));
SELECT @last_id;

SELECT Broj_mjesta_za_putnike, Registracija, Vozilo_Registracija, vrijeme_i_datum_polaska, vrijeme_i_datum_dolaska FROM Vozilo LEFT JOIN Voznja ON Registracija = Vozilo_Registracija;

SET @ime = NULL;
SELECT Ime_stranke INTO @ime FROM Stranka WHERE Ime_stranke = "FK Boracas";
SELECT @ime;

UPDATE uplata
	SET 
		Uplaceni_iznos =  Uplaceni_iznos + 10,
		Datum_uplate = CURRENT_DATE()
WHERE UplataID = (SELECT Uplata_UplataID FROM Voznja WHERE idVoznja = 1);

SELECT * FROM uplata WHERE UplataID = (SELECT Uplata_UplataID FROM Voznja WHERE idVoznja = 1);

SELECT * FROM Voznja WHERE Vozac_idVozac IS NULL;

SELECT * FROM Voznja a LEFT JOIN Uplata b ON b.UplataID = a.Uplata_UplataID WHERE b.Uplaceni_iznos IS NULL OR b.Uplaceni_iznos < b.Ukupni_iznos;

SELECT * FROM Voznja a WHERE a.Vozac_idVozac IS NULL;

SELECT * FROM Voznja a WHERE a.Vozilo_Registracija IS NULL;

SELECT * FROM Voznja a WHERE a.Vozac_idVozac = 1;

SELECT SUM(svi_troskovi.iznos_troska)
	FROM (
		SELECT opis_troska, iznos_troska, vrijeme_i_datum_polaska AS datum
		FROM dodatni_troskovi_voznje a, voznja b
		WHERE YEAR(vrijeme_i_datum_polaska) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(Vrijeme_i_datum_polaska) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) AND b.idvoznja = a.voznja_idvoznja
		UNION
		SELECT a.opis_troska, a.iznos_troska, a.datum_troska
		FROM troskovi_odrzavanja_vozila a, vozilo b
		WHERE YEAR(a.datum_troska) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(a.datum_troska) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) AND b.Registracija = a.Vozilo_registracija
        ) AS svi_troskovi;
        
SELECT Iznos_troska
FROM dodatni_troskovi_voznje a, voznja b
WHERE YEAR(vrijeme_i_datum_polaska) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(Vrijeme_i_datum_polaska) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) AND b.idvoznja = a.voznja_idvoznja
UNION
SELECT a.Iznos_troska
FROM troskovi_odrzavanja_vozila a, vozilo b
WHERE YEAR(a.datum_troska) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(a.datum_troska) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) AND b.Registracija = a.Vozilo_registracija;

SELECT opis_troska, iznos_troska, vrijeme_i_datum_polaska AS datum
FROM dodatni_troskovi_voznje a, voznja b
WHERE YEAR(vrijeme_i_datum_polaska) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(Vrijeme_i_datum_polaska) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) AND b.idvoznja = a.voznja_idvoznja
UNION
SELECT a.opis_troska, a.iznos_troska, a.datum_troska
FROM troskovi_odrzavanja_vozila a, vozilo b
WHERE YEAR(a.datum_troska) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(a.datum_troska) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) AND b.Registracija = a.Vozilo_registracija;