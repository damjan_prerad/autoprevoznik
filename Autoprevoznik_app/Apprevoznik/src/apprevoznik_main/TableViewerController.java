package apprevoznik_main;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.stage.Stage;

import javafx.fxml.FXML;

import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import javafx.beans.property.SimpleStringProperty;

public class TableViewerController extends Application {

	@FXML public ChoiceBox<String> chcbx;
	
	@FXML public Button shtbbtn;
	
	@FXML TableView<List<String>> table;
	
	
	@FXML
	public void ShowTable() {
    	
		table.getItems().clear();
	    table.getColumns().clear();

		int counter = 0;
		String colName[] = new String[100];
		try {
    		Connection conn;
    		Class.forName(Main.DRIVER_CLASS);//.newInstance();
    		conn = DriverManager.getConnection(Main.URL, Main.USERNAME, Main.PASSWORD);
    		
    	    String query = "SELECT * FROM " + chcbx.getValue().toString();
    	    try
    	    {
    	      Statement st = conn.createStatement();
    	      ResultSet rs = st.executeQuery(query);
    	      ResultSetMetaData md = (ResultSetMetaData) rs.getMetaData();
	          counter = md.getColumnCount();
	          colName = new String[counter];
	          //System.out.println("The column names are as follows:");
	          for (int loop = 1; loop <= counter; loop++) {
	        	 //Column names
	        	 TableColumn<List<String>, String> column = new TableColumn<>(md.getColumnLabel(loop));
	        	 final int colIndex = loop - 1;
	        	 column.setCellValueFactory(cellData -> 
	             	new SimpleStringProperty(cellData.getValue().get(colIndex)));
	        	 table.getColumns().add(column);
	        	 colName[loop-1] = md.getColumnLabel(loop);
	             System.out.println(colName[loop-1]);
	          }
	          //System.out.println("Table " + chcbx.getValue().toString() + " ima kolona " + counter);
    	      while (rs.next())
    	      {
    	    	List<String> list = new ArrayList<String>();
	    	    for (int loop = 1; loop <= counter; loop++) {
	    	    	String s = rs.getString(loop);
	    	    	list.add(s);
	            }
    	        //System.out.println(list);
    	        table.getItems().add(FXCollections.observableList(list));
    	        //table.getColumns().add(FXCollections.observableList(list));
    	      }
    	    }
    	    catch (SQLException ex)
    	    {
    	      System.err.println(ex.getMessage());
    	    }
    		conn.close();
    	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}	
	}
	
	@Override
    public void start(Stage stage) {
		
    }
	
	@FXML
    public void initialize() {
		table.getColumns().clear();
		
		//Getting names of all tables in database
    	try {
    		Connection conn;
    		Class.forName(Main.DRIVER_CLASS);//.newInstance();
    		conn = DriverManager.getConnection(Main.URL, Main.USERNAME, Main.PASSWORD);
    		
    	    String query = "SELECT * FROM table_names_list";
    	    try
    	    {
    	      Statement st = conn.createStatement();
    	      ResultSet rs = st.executeQuery(query);
    	      System.out.println("Table Naziv_metoda_uplate");
    	      while (rs.next())
    	      {
    	        String s1 = rs.getString("TABLE_NAME");
    	        chcbx.getItems().add(s1);
    	        //System.out.println(s1);
    	      }
    	    }
    	    catch (SQLException ex)
    	    {
    	      System.err.println(ex.getMessage());
    	    }
    		conn.close();
    	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	chcbx.setValue(chcbx.getItems().get(0));
    }
}
