package apprevoznik_main;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class NewClient extends Application{
	
	@FXML TextField ime;
	@FXML TextField tel;
	@FXML TextField adr;
	
	@FXML Button potvrd;
	
	@FXML Label errlab;
	
	@FXML
	public void enterData() {
		errlab.setText("");
		
		String ime_ = ime.getText();
		String tel_ = tel.getText();
		String adr_ = adr.getText();
		
		System.out.println(ime_ + " " + tel_ + " " + adr_);
		
		try {
    		Connection conn;
    		Class.forName(Main.DRIVER_CLASS);//.newInstance();
    		conn = DriverManager.getConnection(Main.URL, Main.USERNAME, Main.PASSWORD);
    		
    	    try
    	    {    	
	    	  String query = "{ CALL unos_nove_stranke( ?, ?, ?) }";
	    	  CallableStatement stmt = conn.prepareCall(query);
    	      stmt.setString(1, ime_);
    	      stmt.setString(2, tel_);
    	      stmt.setString(3, adr_);
    	      stmt.executeUpdate();
    	    }
    	    catch (SQLException ex)
    	    {
    	      System.err.println(ex.getMessage());
    	      errlab.setText(ex.getMessage());
    	    }
    		conn.close();
    	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
	}
	
	@Override
    public void start(Stage stage) {
		
    }
	
	@FXML
    public void initialize() {
		errlab.setText("");
	}
}
