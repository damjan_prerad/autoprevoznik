package apprevoznik_main;

import java.sql.Timestamp;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class NewRideSetup extends Application{
	
	@FXML public Button uv;
	@FXML public Label el;
	
	@FXML public TextField vpg;
	@FXML public TextField vpm;
	@FXML public TextField vpd;
	@FXML public TextField vps;
	@FXML public TextField vpmin;
	
	@FXML public TextField vdg;
	@FXML public TextField vdm;
	@FXML public TextField vdd;
	@FXML public TextField vds;
	@FXML public TextField vdmin;
	
	@FXML public TextField mp;
	@FXML public TextField od;
	
	@FXML public TextField ud;
	
	@FXML public TextField bp;
	
	@FXML public TextField td;
	
	@FXML public ComboBox nu;
	
	@FXML public TextField iz;
	
	@FXML public CheckBox chcbxup;
	@FXML public TextField uplid;
	
	@FXML public TextField nzvstr;
	
	@FXML public Button nstr;
	
	@FXML
	public void createNewClient() {
		try {
			Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("NewClient.fxml"));
			Scene scene = new Scene(root, 270, 295);
	        
	        Stage stage = new Stage();
	        stage.setTitle("Unos novog klijenta");
	        stage.setScene(scene);
	        
	        stage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void invoiceExists() {
		System.out.println("Hello");
		if(chcbxup.isSelected()) {
			iz.setEditable(false);
			//nu.setEditable(false);
			uplid.setEditable(true);
			nzvstr.setEditable(false);
			
			iz.setDisable(true);
			nu.setDisable(true);
			uplid.setDisable(false);
			nzvstr.setDisable(true);
		}else {
			iz.setEditable(true);
			//nu.setEditable(true);
			uplid.setEditable(false);
			nzvstr.setEditable(true);
			
			iz.setDisable(false);
			nu.setDisable(false);
			uplid.setDisable(true);
			nzvstr.setDisable(false);
		}
	}
	
	@FXML
	public void enterRide() {
		el.setText("");
		if(!chcbxup.isSelected()) {
			if(vpg.getText().trim().isEmpty()|| vpm.getText().trim().isEmpty() || vpd.getText().trim().isEmpty() ||
			   vps.getText().trim().isEmpty() || vpmin.getText().trim().isEmpty() || vdg.getText().trim().isEmpty() ||
			   vdm.getText().trim().isEmpty() || vdd.getText().trim().isEmpty() || vds.getText().trim().isEmpty() ||
			   vdmin.getText().trim().isEmpty() || mp.getText().trim().isEmpty() || od.getText().trim().isEmpty() ||
			   ud.getText().trim().isEmpty() || bp.getText().trim().isEmpty() || td.getText().trim().isEmpty() || iz.getText().trim().isEmpty()) {
			   el.setText("Neko od polja je ostalo prazno!");
			   return;
			}
			
			String mjesto_polaska = mp.getText();
			String odrediste = od.getText();
			int udaljenost = Integer.parseInt(ud.getText());
			int broj_putnika = Integer.parseInt(bp.getText());
			String tip_destinacije = td.getText();
			float iznos = Float.parseFloat(iz.getText());
			String nup = nu.getValue().toString();
			String naziv_stranke = nzvstr.getText();
			
			try {
	    		Connection conn;
	    		Class.forName(Main.DRIVER_CLASS);//.newInstance();
	    		conn = DriverManager.getConnection(Main.URL, Main.USERNAME, Main.PASSWORD);
	    		
	    	    try
	    	    {
	    	      String query = "{ CALL nadji_id_metoda_uplate( ?, ?) }";
	      	      CallableStatement stmt = conn.prepareCall(query);
	      	      stmt.setString(1, nup);
	      	      stmt.registerOutParameter(2, Types.INTEGER);
	      	      stmt.executeUpdate();
	      	      int id = stmt.getInt(2);

		    	  query = "{ CALL organizacija_nove_voznje( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }";
	    	      stmt = conn.prepareCall(query);
	    	      stmt.setFloat(1, iznos);
	    	      stmt.setDate(2, java.sql.Date.valueOf(vpg.getText() + "-" + vpm.getText() + "-" + vpd.getText()));
	    	      stmt.setInt(3, id);
	    	      stmt.setString(4, naziv_stranke);
	    	      stmt.setTimestamp(5, java.sql.Timestamp.valueOf(vpg.getText() + "-" + vpm.getText() + "-" + vpd.getText() + " " + (Integer.parseInt(vps.getText()) + 2) + ":" + vpmin.getText() + ":00"));
	    	      stmt.setTimestamp(6, java.sql.Timestamp.valueOf(vdg.getText() + "-" + vdm.getText() + "-" + vdd.getText() + " " + (Integer.parseInt(vds.getText()) + 2) + ":" + vdmin.getText() + ":00"));
	    	      stmt.setString(7, mjesto_polaska);
	    	      stmt.setString(8, odrediste);
	    	      stmt.setInt(9,  udaljenost);
	    	      stmt.setInt(10, broj_putnika);
	    	      stmt.setString(11, tip_destinacije);
	    	      stmt.executeUpdate();
	    	      
	    	    }
	    	    catch (SQLException ex)
	    	    {
	    	      System.err.println(ex.getMessage());
	    	      el.setText(ex.getMessage());
	    	    }
	    		conn.close();
	    	}
	    	catch(Exception ex){
	    		ex.printStackTrace();
	    	}
		}
		else {
			String mjesto_polaska = mp.getText();
			String odrediste = od.getText();
			int udaljenost = Integer.parseInt(ud.getText());
			int broj_putnika = Integer.parseInt(bp.getText());
			String tip_destinacije = td.getText();
			String nup = nu.getValue().toString();
			int id_uplate = Integer.parseInt(uplid.getText());
			
			try {
	    		Connection conn;
	    		Class.forName(Main.DRIVER_CLASS);//.newInstance();
	    		conn = DriverManager.getConnection(Main.URL, Main.USERNAME, Main.PASSWORD);
	    		
	    	    try
	    	    {    	
		    	  String query = "{ CALL organizacija_nove_voznje_na_postojecu_uplatu( ?, ?, ?, ?, ?, ?, ?, ?) }";
		    	  CallableStatement stmt = conn.prepareCall(query);
	    	      stmt.setTimestamp(1, java.sql.Timestamp.valueOf(vpg.getText() + "-" + vpm.getText() + "-" + vpd.getText() + " " + (Integer.parseInt(vps.getText()) + 2) + ":" + vpmin.getText() + ":00"));
	    	      stmt.setTimestamp(2, java.sql.Timestamp.valueOf(vdg.getText() + "-" + vdm.getText() + "-" + vdd.getText() + " " + (Integer.parseInt(vds.getText()) + 2) + ":" + vdmin.getText() + ":00"));
	    	      stmt.setString(3, mjesto_polaska);
	    	      stmt.setString(4, odrediste);
	    	      stmt.setInt(5,  udaljenost);
	    	      stmt.setInt(6, broj_putnika);
	    	      stmt.setString(7, tip_destinacije);
	    	      stmt.setInt(8, id_uplate);
	    	      stmt.executeUpdate();
	    	    }
	    	    catch (SQLException ex)
	    	    {
	    	      System.err.println(ex.getMessage());
	    	      el.setText(ex.getMessage());
	    	    }
	    		conn.close();
	    	}
	    	catch(Exception ex){
	    		ex.printStackTrace();
	    	}
		}
	}
	
	@Override
    public void start(Stage stage) {
		
    }
	
	@FXML
    public void initialize() {
		el.setText("");
		
		iz.setEditable(true);
		nu.setEditable(false);
		uplid.setEditable(false);
		nzvstr.setEditable(true);
		
		iz.setDisable(false);
		nu.setDisable(false);
		uplid.setDisable(true);
		nzvstr.setDisable(false);
		
		try {
    		Connection conn;
    		Class.forName(Main.DRIVER_CLASS);//.newInstance();
    		conn = DriverManager.getConnection(Main.URL, Main.USERNAME, Main.PASSWORD);
    		
    	    String query = "SELECT * FROM Metod_uplate";
    	    try
    	    {
    	      Statement st = conn.createStatement();
    	      ResultSet rs = st.executeQuery(query);
    	      while (rs.next())
    	      {
    	        String s1 = rs.getString(2);
    	        nu.getItems().add(s1);
    	        //System.out.println(s1);
    	      }
    	    }
    	    catch (SQLException ex)
    	    {
    	      System.err.println(ex.getMessage());
    	    }
    		conn.close();
    	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	nu.setValue(nu.getItems().get(0));
    }
}
